import Vue from 'vue'

import LoadingIcon from '~/components/common/loading-icon'
Vue.component('LoadingIcon', LoadingIcon)

import CopangPeperoInfo from '~/components/copang-pepero-info'
Vue.component('CopangPeperoInfo', CopangPeperoInfo)
